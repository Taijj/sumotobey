﻿using Taijj.Utils;
using UnityEngine;

namespace Taijj.SumoTobey
{
    /// <summary>
    /// Top-level flow controller.
    /// </summary>
    public class App : MonoBehaviour
    {
        #region Init
        [SerializeField] private Game m_game;
        [SerializeField] private UserInterface m_userInterface;

        public void Awake()
        {
            m_game.wake();
            m_userInterface.wake();

            s_userInterface = m_userInterface;
        }

        public void Start()
        {
            PlayerInput.instance.Activate();
            ExitHandler.instance.Activate();
            showStartScreen();
        }

        public static UserInterface s_userInterface { get; private set; }
        #endregion



        #region Start Screen
        private void showStartScreen()
        {
            m_userInterface.startScreen.evOnCompleted += onStartScreenCompleted;
            m_userInterface.startScreen.show();
        }

        private void onStartScreenCompleted()
        {
            m_userInterface.startScreen.evOnCompleted -= onStartScreenCompleted;
            m_userInterface.startScreen.hide();

            startGame();
        }
        #endregion



        #region Game
        private void startGame()
        {
            m_game.evOnGameEnded += onGameEnded;
            m_game.startGame();

            m_userInterface.gameScreen.show();
        }

        private void onGameEnded(Player winner)
        {
            m_game.evOnGameEnded -= onGameEnded;
            m_userInterface.gameScreen.hide();

            showEndScreen(winner.name);
        }
        #endregion



        #region End Screen
        private void showEndScreen(string _strWinnerName)
        {
            m_userInterface.endScreen.evOnCompleted += onEndScreenCompleted;
            m_userInterface.endScreen.Feed(_strWinnerName);
            m_userInterface.endScreen.show();
        }

        private void onEndScreenCompleted()
        {
            m_userInterface.endScreen.evOnCompleted -= onEndScreenCompleted;
            m_userInterface.endScreen.hide();

            startGame();
        }
        #endregion
    }
}