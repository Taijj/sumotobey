﻿using UnityEngine;

namespace Taijj.SumoTobey
{
    public class Fan : MimiBehaviour
    {
        public void OnTriggerEnter2D(Collider2D _col)
        {
            bool bIsCharacter = _col.TryGetComponent<Character>(out Character character);
            if(bIsCharacter)
            {
                Vector2 v2ForceDirection = (Vector2)(character.trans.position - trans.position);
                character.rigid.AddForce(v2ForceDirection, ForceMode2D.Impulse);
                character.onDamage();
            }
        }
    }
}