﻿using System;
using System.Collections;
using UnityEngine;

namespace Taijj.SumoTobey
{
    /// <summary>
    /// Flow controller of the game loop.
    /// </summary>
    public class Game : MonoBehaviour
    {
        #region Init
        public event Action<Player> evOnGameEnded;

        [SerializeField] private Player m_player1;
        [SerializeField] private Player m_player2;
        [SerializeField] private Tripod m_tripod;

        public void wake()
        {
            m_player1.wake(1);
            m_player2.wake(2);
            m_tripod.wake();
            gameObject.SetActive(false);
        }
        #endregion



        #region Game Lifecycle
        public void startGame()
        {
            gameObject.SetActive(true);
            m_player1.prepare();
            m_player2.prepare();
            m_tripod.prepare();
            App.s_userInterface.gameScreen.prepare(m_player1, m_player2);

            m_player1.evOnHealthChanged += onPlayerHealthChanged;
            m_player2.evOnHealthChanged += onPlayerHealthChanged;
        }

        private void endGame()
        {
            m_player1.cleanUp();
            m_player2.cleanUp();
            m_tripod.cleanUp();
            App.s_userInterface.gameScreen.cleanUp();

            m_player1.evOnHealthChanged -= onPlayerHealthChanged;
            m_player2.evOnHealthChanged -= onPlayerHealthChanged;
        }

        private void onPlayerHealthChanged()
        {
            if(m_player1.iCurrentHitPoints > 0 && m_player2.iCurrentHitPoints > 0)
            {
                return;
            }

            endGame();
            StartCoroutine(delayAndComplete());
        }

        private IEnumerator delayAndComplete()
        {
            yield return new WaitForSeconds(0.5f);
            gameObject.SetActive(false);

            Player winner = m_player1.iCurrentHitPoints > 0 ? m_player1 : m_player2;
            evOnGameEnded?.Invoke(winner);
        }
        #endregion
    }
}