﻿using System;
using UnityEngine;

namespace Taijj.SumoTobey
{
    /// <summary>
    /// A player avatar. The object, that actually moves through the scene
    /// and interacts with physics.
    /// </summary>
    public class Character : MimiBehaviour
    {
        #region Beginning
        private const float c_fSpeedDamping = 100f;
        private const float c_fJumpAmplifier = 20f;
        private const string c_strFloorTag = "Floor";



        [SerializeField] private CharacterAnimator m_animator;

        public void wake(float _fMoveSpeed, float _fJumpForce)
        {
            base.Awake();

            rigid = GetComponent<Rigidbody2D>();
            col = GetComponent<BoxCollider2D>();
            v3BaseMovement = Vector3.right * _fMoveSpeed/c_fSpeedDamping;
            v3BaseJumpForce = Vector3.up * _fJumpForce * c_fJumpAmplifier;

            m_animator.wake(this);
        }



        public void prepare()
        {
            m_animator.prepare();
        }

        public void cleanUp()
        {
            m_animator.cleanUp();
        }



        public Rigidbody2D rigid { get; private set; }
        private BoxCollider2D col { get; set; }
        #endregion



        #region Movement
        public void Move(int _iDirection, float _fMaxVelocity)
        {
            bool bIsTooFast = Mathf.Abs(rigid.velocity.x) > _fMaxVelocity;
            if(bIsTooFast)
            {
                return;
            }

            Vector3 v3Movement = v3BaseMovement*(float)_iDirection;
            rigid.AddForce(v3Movement, ForceMode2D.Impulse);
            m_animator.setDirection(_iDirection);
        }

        public void Jump()
        {
            if(bIsGrounded == false)
            {
                return;
            }

            rigid.AddForce(v3BaseJumpForce, ForceMode2D.Force);
        }

        public void decelerate(float _fFactor)
        {
            Vector2 v2Velocity = rigid.velocity;
            rigid.velocity = new Vector2(v2Velocity.x*_fFactor, v2Velocity.y);
        }

        private Vector3 v3BaseMovement { get; set; }
        private Vector3 v3BaseJumpForce { get; set; }
        #endregion



        #region Grounding
        public void OnCollisionEnter2D(Collision2D _collision)
        {
            if(bIsFloorCollision(_collision))
            {
                bIsGrounded = true;
            }
            checkDamage(_collision);
        }

        public void OnCollisionExit2D(Collision2D _collision)
        {
            if(bIsFloorCollision(_collision))
            {
                bIsGrounded = false;
            }
        }


        private bool bIsFloorCollision(Collision2D _collision)
        {
            return string.Equals(_collision.gameObject.tag, c_strFloorTag);
        }


        public bool bIsGrounded { get; private set; }
        #endregion



        #region Stomping
        public event Action evOnDamaged;

        private void checkDamage(Collision2D _collision)
        {
            bool bIsCharacterCollision = _collision.gameObject.TryGetComponent<Character>(out Character other);
            if(bIsCharacterCollision == false)
            {
                return;
            }

            float fHalfColliderHeight = col.size.y/2f;
            Vector3 v3OtherPosition = other.trans.position;
            bool bIsBelowThis = v3OtherPosition.y < trans.position.y+fHalfColliderHeight;
            if(bIsBelowThis)
            {
                return;
            }

            float fColliderWidht = col.size.x;
            bool bIsWithinMargins = v3OtherPosition.x > trans.position.x - fColliderWidht &&
                                    v3OtherPosition.x < trans.position.x + fColliderWidht;
            if(bIsWithinMargins)
            {
                other.rigid.AddForce(v3BaseJumpForce, ForceMode2D.Force);
                onDamage();
            }
        }

        public void onDamage()
        {
            m_animator.triggerDamage();
            evOnDamaged?.Invoke();
        }
        #endregion
    }
}