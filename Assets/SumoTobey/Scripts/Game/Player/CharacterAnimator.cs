﻿using System.Collections;
using Taijj.Utils;
using UnityEngine;

namespace Taijj.SumoTobey
{
    /// <summary>
    /// Controls the animation states of a Character.
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class CharacterAnimator : MimiBehaviour
    {
        #region Init
        [SerializeField] private string m_strGroundedBool;
        [SerializeField] private string m_strMovingBool;
        [SerializeField] private string m_strDamageBool;
        [SerializeField] private float m_fDamageCooldownSeconds;

        public void wake(Character _character)
        {
            character = _character;
            animator = GetComponent<Animator>();
            animator.keepAnimatorControllerStateOnDisable = true;
        }

        private Animator animator { get; set; }
        private Character character { get; set; }
        #endregion



        #region Animation States
        public void prepare()
        {
            UpdateCaller.instance.startCallingOnUpdate(updateState);
        }

        public void cleanUp()
        {
            UpdateCaller.instance.stopCallingOnUpdate(updateState);
        }

        private void updateState()
        {
            bool bIsMoving = Mathf.Abs(character.rigid.velocity.x) > 0;

            animator.SetBool(m_strGroundedBool, character.bIsGrounded);
            animator.SetBool(m_strMovingBool, bIsMoving);
        }

        public void setDirection(int _iDirection)
        {
            trans.localScale = new Vector3(_iDirection, 1, 1);
        }



        public void triggerDamage()
        {
            animator.SetBool(m_strDamageBool, true);

            StopCoroutine(coolDownDamage());
            StartCoroutine(coolDownDamage());
        }

        private IEnumerator coolDownDamage()
        {
            yield return new WaitForSeconds(m_fDamageCooldownSeconds);
            animator.SetBool(m_strDamageBool, false);
        }
        #endregion
    }
}