﻿using System;
using Taijj.Utils;
using UnityEngine;

namespace Taijj.SumoTobey
{
    /// <summary>
    /// Top-level controller of player/user.
    /// Forwards input to it's Character, manages it's lifecycle and health.
    /// Doesn't move through the scene itself!
    /// </summary>
    public class Player : MonoBehaviour
    {
        #region Init
        [SerializeField] private float m_fMoveAcceleration;
        [SerializeField] [Range(0, 1)] private float m_fMoveDecelerationFactor;
        [SerializeField] private float m_fMaxMoveVelocity;
        [SerializeField] private float m_fJumpForce;
        [SerializeField] private int m_iMaxHitPoints;
        [Space]
        [SerializeField] private Character m_character;

        public void wake(int _iPlayerIndex)
        {
            iPlayerIndex = _iPlayerIndex;
            m_character.wake(m_fMoveAcceleration, m_fJumpForce);
            v3InitialPosition = m_character.trans.position;
        }

        public void prepare()
        {
            if(iPlayerIndex == 1)
            {
                PlayerInput.s_evOnMovePlayer1 += onMove;
                PlayerInput.s_evOnJumpPlayer1 += onJump;
            }
            else
            {
                PlayerInput.s_evOnMovePlayer2 += onMove;
                PlayerInput.s_evOnJumpPlayer2 += onJump;
            }
            m_character.prepare();
            m_character.evOnDamaged += onGotStomped;
            m_character.trans.position = v3InitialPosition;
            fullyHeal();

            UpdateCaller.instance.startCallingOnUpdate(tryDecelerate);
        }

        public void cleanUp()
        {
            if(iPlayerIndex == 1)
            {
                PlayerInput.s_evOnMovePlayer1 -= onMove;
                PlayerInput.s_evOnJumpPlayer1 -= onJump;
            }
            else
            {
                PlayerInput.s_evOnMovePlayer2 -= onMove;
                PlayerInput.s_evOnJumpPlayer2 -= onJump;
            }
            m_character.cleanUp();
            m_character.evOnDamaged -= onGotStomped;
            UpdateCaller.instance.stopCallingOnUpdate(tryDecelerate);
        }

        public int iPlayerIndex { get; set; }
        private Vector3 v3InitialPosition { get; set; }
        #endregion



        #region Movement
        private void onMove(int _iDirection)
        {
            m_character.Move(_iDirection, m_fMaxMoveVelocity);
        }

        private void onJump()
        {
            m_character.Jump();
        }

        private void tryDecelerate()
        {
            if(PlayerInput.instance.bIsAnyKeyHeld)
            {
                return;
            }
            m_character.decelerate(1f-m_fMoveDecelerationFactor);
        }
        #endregion



        #region Damage
        public event Action evOnHealthChanged;

        private void fullyHeal()
        {
            iCurrentHitPoints = m_iMaxHitPoints;
            evOnHealthChanged?.Invoke();
        }

        private void onGotStomped()
        {
            iCurrentHitPoints = Mathf.Max(iCurrentHitPoints-1, 0);
            evOnHealthChanged?.Invoke();
        }

        public int iCurrentHitPoints { get; set; }
        public int iMaxHitPoints => m_iMaxHitPoints;
        #endregion
    }
}