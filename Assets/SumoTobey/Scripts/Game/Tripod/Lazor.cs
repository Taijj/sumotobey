﻿using System;
using UnityEngine;

namespace Taijj.SumoTobey.Tripods
{
    /// <summary>
    /// The Component managing the Lazor animation, how it will be shown,
    /// and the damage it does.
    /// </summary>
    public class Lazor : MimiBehaviour
    {
        #region Init
        public event Action evOnImmpact;
        public event Action evOnCompleted;

        [SerializeField] private SpriteRenderer m_renderer;
        [SerializeField] private string m_insideSortingLayerName;
        [SerializeField] private string m_outsideSortingLayerName;
        [Space]
        [SerializeField] private Rect m_rectDamageZone;
        [SerializeField] private float m_fExplosionForce;

        public void wake()
        {
            base.Awake();
            arHitColliders = new Collider2D[4];
            setPositions(trans);
            hide();
        }

        private void setPositions(Transform _trans)
        {
            v2DamageZoneCenter = (Vector2)_trans.position + m_rectDamageZone.center;
            v2Epicenter = v2DamageZoneCenter + Vector2.down * m_rectDamageZone.height/2f;
        }
        #endregion



        #region Show/Hide
        public void show(bool _bInside)
        {
            bIsInside = _bInside;

            m_renderer.sortingLayerName = bIsInside ? m_insideSortingLayerName : m_outsideSortingLayerName;
            m_renderer.sortingOrder = 1;
            trans.localScale = new Vector3(bIsInside ? 1 : -1, 1, 1);
            gameObject.SetActive(true);
        }

        public void hide()
        {
            gameObject.SetActive(false);
        }


        public void OnImpact() // Animation Event Target
        {
            if(bIsInside)
            {
                tryDamageCharacters();
            }
            evOnImmpact?.Invoke();
        }

        public void OnCompleted() // Animation Event Target
        {
            evOnCompleted?.Invoke();
        }
        #endregion



        #region Damage
        private void tryDamageCharacters()
        {
            Physics2D.OverlapBoxNonAlloc(v2DamageZoneCenter, m_rectDamageZone.size, 0f, arHitColliders);
            foreach(Collider2D col in arHitColliders)
            {
                if(ReferenceEquals(col, null))
                {
                    continue;
                }
                tryDamage(col);
            }
        }

        private void tryDamage(Collider2D _col)
        {
            bool bIsCharacter = _col.TryGetComponent<Character>(out Character character);
            if(bIsCharacter)
            {
                Vector2 v2ForceDirection = (Vector2)character.trans.position - v2Epicenter;
                character.rigid.AddForce(v2ForceDirection*m_fExplosionForce, ForceMode2D.Impulse);
                character.onDamage();

            }
        }
        #endregion



        #region Properties
        private Vector2 v2DamageZoneCenter { get; set; }
        private Vector2 v2Epicenter { get; set; }
        private Collider2D[] arHitColliders { get; set; }
        private bool bIsInside { get; set; }
        #endregion



        #if UNITY_EDITOR || DEVELOPMENT_BUILD
        void OnDrawGizmosSelected()
        {
            setPositions(transform);
            Gizmos.color = Color.white;
            Gizmos.DrawWireCube(v2DamageZoneCenter, m_rectDamageZone.size);
        }
        #endif
    }
}