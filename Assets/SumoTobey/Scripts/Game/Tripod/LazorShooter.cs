﻿using System;
using System.Collections;
using UnityEngine;

namespace Taijj.SumoTobey.Tripods
{
    /// <summary>
    /// Will idle for a random time interval before
    /// triggering an event for shooting Lazors.
    ///
    /// Manages showing of Lazors.
    /// </summary>
    public class LazorShooter : MonoBehaviour
    {
        #region Init
        public event Action<bool> evOnShootLazor;
        public event Action evOnLazorShot;

        [SerializeField] private Lazor m_lazor;
        [SerializeField] private float m_fMinIdleSeconds;
        [SerializeField] private float m_fMaxIdleSeconds;

        public void wake()
        {
            m_lazor.wake();
        }

        public void cleanUp()
        {
            StopCoroutine(idleRoutine);
        }
        #endregion



        #region Idling
        public void startIdling()
        {
            idleRoutine = StartCoroutine(idle());
        }

        private IEnumerator idle()
        {
            float fIdleSeconds = UnityEngine.Random.Range(m_fMinIdleSeconds, m_fMaxIdleSeconds);
            yield return new WaitForSeconds(fIdleSeconds);

            bool bShootInside = UnityEngine.Random.value > 0.5f;
            evOnShootLazor?.Invoke(bShootInside);
        }

        private Coroutine idleRoutine { get; set; }
        #endregion



        #region Lazor
        public void shootLazor(bool _bInside)
        {
            m_lazor.evOnCompleted += onLazorCompleted;
            m_lazor.show(_bInside);
        }

        private void onLazorCompleted()
        {
            m_lazor.evOnCompleted -= onLazorCompleted;
            m_lazor.hide();
            evOnLazorShot?.Invoke();
        }
        #endregion
    }
}