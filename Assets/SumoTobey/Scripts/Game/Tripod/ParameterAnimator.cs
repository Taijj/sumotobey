﻿using System.Collections;
using Taijj.Utils;
using UnityEngine;

namespace Taijj.SumoTobey.Tripods
{
    /// <summary>
    /// Interpolates between two random fractional(0 to 1) parameters
    /// in a set time interval.
    /// </summary>
    public class ParameterAnimator : MonoBehaviour
    {
        #region Life Cycle
        private const float c_fMinParametersDifference = 0.3f;
        [SerializeField] private float m_fMinSeconds;
        [SerializeField] private float m_fMaxSeconds;
        [SerializeField] private float m_fLerpSpeed;

        public void Wake()
        {
            initializeLerping();
        }

        public void cleanUp()
        {
            stop();
        }
        #endregion



        #region Play/Stop
        public void play()
        {
            fNextParameter = fParameter;
            initializeLerping();
            startNext();
        }

        private void stop()
        {
            StopCoroutine(timeRoutine);
        }
        #endregion



        #region Time Coroutine
        private void startNext()
        {
            fLastParameter = fNextParameter;
            fSetRandomNextParameter();

            fLerpTime = 0f;
            timeRoutine = StartCoroutine(waitRandomTime());

            Utils.Logger.log($"Started Next - Last {fLastParameter} Next {fNextParameter}");
        }

        private void fSetRandomNextParameter()
        {
            bool bIsTooClose = false;
            WhileCounter.OnWhileLoopStart(100);
            do
            {
                fNextParameter = Random.value;
                bIsTooClose = Mathf.Abs(fNextParameter-fLastParameter) < c_fMinParametersDifference;
                WhileCounter.OnWhileLoopIteration();
            } while(bIsTooClose);
        }

        private IEnumerator waitRandomTime()
        {
            float fSeconds = Random.Range(m_fMinSeconds, m_fMaxSeconds);
            yield return new WaitForSeconds(fSeconds);
            startNext();
        }

        private Coroutine timeRoutine { get; set; }
        #endregion



        #region Lerping
        private const float c_fLerpParameterDamper = 100f;

        private void initializeLerping()
        {
            fLerpSpeed = m_fLerpSpeed / c_fLerpParameterDamper;
        }

        public void lerpParameter()
        {
            if(fLerpTime >= 1f)
            {
                return;
            }

            fParameter = Mathf.Lerp(fLastParameter, fNextParameter, fLerpTime);
            fLerpTime += fLerpSpeed;
        }



        public float fParameter { get; private set; }

        private float fNextParameter { get; set;}
        private float fLastParameter { get; set; }
        private float fLerpTime { get; set; }
        private float fLerpSpeed { get; set; }
        #endregion



        #region Force
        private const float c_fInsideParameter = 0.75f;
        private const float c_fOutsideParameter = 0f;

        public void setToAndStop(bool _bInsdie)
        {
            stop();

            fLastParameter = fParameter;
            fNextParameter = _bInsdie ? c_fInsideParameter : c_fOutsideParameter;
            fLerpTime = 0f;
        }
        #endregion
    }
}