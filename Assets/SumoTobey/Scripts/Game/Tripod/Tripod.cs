﻿using UnityEngine;
using Taijj.SumoTobey.Tripods;
using Taijj.Utils;

namespace Taijj.SumoTobey
{
    /// <summary>
    /// Top level flow controller of the lazor shooting background tripod.
    /// </summary>
    public class Tripod : MonoBehaviour
    {
        #region Init
        [SerializeField] private TripodHead m_head;
        [SerializeField] private ParameterAnimator m_animator;
        [SerializeField] private LazorShooter m_shooter;

        public void wake()
        {
            m_head.wake();
            m_shooter.wake();
        }

        public void prepare()
        {
            m_shooter.evOnShootLazor += onShootLazor;
            m_shooter.evOnLazorShot += onLazorShot;
            UpdateCaller.instance.startCallingOnUpdate(update);

            m_shooter.startIdling();
            m_animator.play();
        }

        public void cleanUp()
        {
            m_shooter.evOnShootLazor -= onShootLazor;
            m_shooter.evOnLazorShot -= onLazorShot;
            UpdateCaller.instance.stopCallingOnUpdate(update);

            m_shooter.cleanUp();
            m_animator.cleanUp();
        }
        #endregion



        private void update()
        {
            m_animator.lerpParameter();
            m_head.updateSprites(m_animator.fParameter);
        }

        private void onShootLazor(bool _bInside)
        {
            //TODO wait until animtor is finished with lerping before shooting Lazor
            m_animator.setToAndStop(_bInside);
            m_shooter.shootLazor(_bInside);
        }

        private void onLazorShot()
        {
            m_animator.play();
            m_shooter.startIdling();
        }
    }
}