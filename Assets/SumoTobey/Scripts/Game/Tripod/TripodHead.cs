﻿using System.Collections.Generic;
using UnityEngine;

namespace Taijj.SumoTobey.Tripods
{
    /// <summary>
    /// Head of the tripod in the background.
    ///
    /// Changes its Sprite depending on a position on a
    /// line in the Scene. That position is derived from
    /// a fractional 0 to 1 parameter.
    /// </summary>
    public class TripodHead : MimiBehaviour
    {
        #region Init
        [SerializeField] private List<Sprite> m_liSprites;
        [SerializeField] private float m_fLookArea;
        [SerializeField] private float m_fLookAreaDistance;
        [Space]
        [Range(0, 1)] public float m_fParameter;



        public void wake()
        {
            base.Awake();
            cacheProperties(trans.position);
            calculateSpriteAngles();
        }

        private void cacheProperties(Vector3 _v3Position)
        {
            spriteRenderer = GetComponent<SpriteRenderer>();

            v2Position = (Vector2)_v3Position;
            fAreaHalfWidth = m_fLookArea/2f;
            fAreaPointY = v2Position.y + m_fLookAreaDistance;
            v2AreaLeftVertex = new Vector2(v2Position.x-fAreaHalfWidth, fAreaPointY);
            v2AreaRightVertex = new Vector2(v2Position.x+fAreaHalfWidth, fAreaPointY);
        }

        private void calculateSpriteAngles()
        {
            liSpriteAngles = new List<float>();

            int iTotalParts = m_liSprites.Count+1;
            float fSinglePartWidth = m_fLookArea/(float)iTotalParts;
            float fSinglePartFraction = fSinglePartWidth/m_fLookArea;
            for(int i = 0; i < iTotalParts; i++)
            {
                float fParameter = (float)i * fSinglePartFraction;
                float angle = fGetAngleAt(fParameter);
                liSpriteAngles.Add(angle);
            }
        }
        #endregion



        #region Sprites Update
        public void updateSprites(float _fParameter)
        {
            m_fParameter = _fParameter;
            float fCurrentAngle = fGetAngleAt(m_fParameter);

            for(int i = 1; i < m_liSprites.Count+1; i++)
            {
                if(fCurrentAngle < liSpriteAngles[i])
                {
                    spriteRenderer.sprite = m_liSprites[i-1];
                    break;
                }
            }
        }

        private float fGetAngleAt(float _fParameter)
        {
            Vector2 v2Point = v2GetPointOnArea(_fParameter);
            return Vector2.SignedAngle(Vector2.down, v2Point);
        }

        private Vector2 v2GetPointOnArea(float _fParameter)
        {
            float fX = v2AreaLeftVertex.x + m_fLookArea*_fParameter;
            return new Vector2(fX, fAreaPointY);
        }
        #endregion



        #region Properties
        private SpriteRenderer spriteRenderer { get; set; }
        private Vector2 v2Position { get; set; }
        private Vector2 v2AreaLeftVertex { get; set; }
        private Vector2 v2AreaRightVertex { get; set; }
        private List<float> liSpriteAngles { get; set; }

        private float fAreaHalfWidth { get; set; }
        private float fAreaPointY { get; set; }
        #endregion



        #if UNITY_EDITOR || DEVELOPMENT_BUILD
        void OnDrawGizmosSelected()
        {
            cacheProperties(transform.position);

            Gizmos.color = Color.white;
            Gizmos.DrawLine(v2AreaLeftVertex, v2AreaRightVertex);

            Gizmos.color = Color.green;
            Vector2 v2PointOnArea = v2GetPointOnArea(m_fParameter);
            Gizmos.DrawSphere(v2PointOnArea, 0.1f);
        }
        #endif
    }
}