﻿using UnityEngine;
using Taijj.Utils;
using System;

namespace Taijj.SumoTobey
{
    /// <summary>
    /// Singleton that provides user-input events to be subscribed to from anywhere.
    /// </summary>
    public class PlayerInput : SceneSingleton<PlayerInput>
    {
        #region Events
        public static event Action<int> s_evOnMovePlayer1;
        public static event Action s_evOnJumpPlayer1;
        public static event Action<int> s_evOnMovePlayer2;
        public static event Action s_evOnJumpPlayer2;

        public static event Action s_evOnAnyKey;
        #endregion



        #region Activations
        public void Activate()
        {
            UpdateCaller.instance.startCallingOnUpdate(checkInput);
        }

        public void Deactivate()
        {
            UpdateCaller.instance.stopCallingOnUpdate(checkInput);
        }
        #endregion



        #region Input
        public void checkInput()
        {
            checkForMove();
            checkForJump();
            checkAnyKey();
        }

        private void checkForMove()
        {
            if(bLeftIsHeldPlayer1)
            {
                s_evOnMovePlayer1?.Invoke(-1);
            }
            if(bRightIsHeldPlayer1)
            {
                s_evOnMovePlayer1?.Invoke(1);
            }
            if(bLeftIsHeldPlayer2)
            {
                s_evOnMovePlayer2?.Invoke(-1);
            }
            if(bRightIsHeldPlayer2)
            {
                s_evOnMovePlayer2?.Invoke(1);
            }
        }

        private void checkForJump()
        {
            if(bJumpIsTriggerdPlayer1)
            {
                s_evOnJumpPlayer1?.Invoke();
            }
            if(bJumpIsTriggerdPlayer2)
            {
                s_evOnJumpPlayer2?.Invoke();
            }
        }

        private void checkAnyKey()
        {
            if(bIsAnyKeyTriggered)
            {
                s_evOnAnyKey?.Invoke();
            }
        }
        #endregion




        #region Key Mapping
        private bool bLeftIsHeldPlayer1 => Input.GetKey(KeyCode.A);
        private bool bRightIsHeldPlayer1 => Input.GetKey(KeyCode.D);
        private bool bJumpIsTriggerdPlayer1 => Input.GetKeyDown(KeyCode.W);

        private bool bLeftIsHeldPlayer2 => Input.GetKey(KeyCode.LeftArrow);
        private bool bRightIsHeldPlayer2 => Input.GetKey(KeyCode.RightArrow);
        private bool bJumpIsTriggerdPlayer2 => Input.GetKeyDown(KeyCode.UpArrow);

        private bool bIsAnyKeyTriggered => Input.anyKeyDown;
        public bool bIsAnyKeyHeld => Input.anyKey;
        #endregion
    }
}