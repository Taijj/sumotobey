﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;

namespace Taijj.SumoTobey
{
    /// <summary>
    /// Screen that shows the round's winner.
    /// </summary>
    public class EndScreen : Screen
    {
        public event Action evOnCompleted;
        [SerializeField] private TextMeshProUGUI m_textMesh;

        public void Feed(string _strWinnerName)
        {
            m_textMesh.text = $"{_strWinnerName} has won!";
        }

        protected override void onShown()
        {
            // To prevent instant close
            StartCoroutine(delayInputActivation());
        }

        private IEnumerator delayInputActivation()
        {
            yield return new WaitForSeconds(1f);
            PlayerInput.s_evOnAnyKey += complete;
        }

        private void complete()
        {
            PlayerInput.s_evOnAnyKey -= complete;
            evOnCompleted?.Invoke();
        }

    }
}