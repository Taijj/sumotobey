﻿using UnityEngine;

namespace Taijj.SumoTobey
{
    /// <summary>
    /// Screen that is shown during Gameplay.
    /// </summary>
    public class GameScreen : Screen
    {
        [SerializeField] private HealthPanel m_healthPanel1;
        [SerializeField] private HealthPanel m_healthPanel2;

        public void prepare(Player _player1, Player _player2)
        {
            m_healthPanel1.prepare(_player1);
            m_healthPanel2.prepare(_player2);
        }

        public void cleanUp()
        {
            m_healthPanel1.cleanUp();
            m_healthPanel2.cleanUp();
        }
    }
}