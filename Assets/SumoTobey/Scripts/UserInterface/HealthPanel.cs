﻿using UnityEngine;

namespace Taijj.SumoTobey
{
    /// <summary>
    /// Panel showing a player's health.
    /// </summary>
    public class HealthPanel : MonoBehaviour
    {
        [SerializeField] private Transform m_transFill;

        public void prepare(Player _player)
        {
            player = _player;
            player.evOnHealthChanged += updateFill;
            updateFill();
        }

        public void cleanUp()
        {
            player.evOnHealthChanged -= updateFill;
        }

        private void updateFill()
        {
            float fPercent = (float)player.iCurrentHitPoints/(float)player.iMaxHitPoints;
            m_transFill.localScale = new Vector3(fPercent, 1f, 1f);
        }

        private Player player { get; set; }
    }
}