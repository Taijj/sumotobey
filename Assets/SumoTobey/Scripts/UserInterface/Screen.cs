﻿namespace Taijj.SumoTobey
{
    /// <summary>
    /// Screen base Component.
    /// </summary>
    public class Screen : MimiBehaviour
    {
        public virtual void wake()
        {
            gameObject.SetActive(false);
        }

        public void show()
        {
            gameObject.SetActive(true);
            onShown();
        }
        protected virtual void onShown(){}

        public void hide()
        {
            gameObject.SetActive(false);
            onHidden();
        }
        protected virtual void onHidden(){}
    }
}