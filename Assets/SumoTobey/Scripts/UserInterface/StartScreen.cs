﻿using System;

namespace Taijj.SumoTobey
{
    /// <summary>
    /// The first screen shown.
    /// </summary>
    public class StartScreen : Screen
    {
        public event Action evOnCompleted;

        protected override void onShown()
        {
            PlayerInput.s_evOnAnyKey += complete;
        }

        private void complete()
        {
            PlayerInput.s_evOnAnyKey -= complete;
            evOnCompleted?.Invoke();
        }

    }
}