﻿using UnityEngine;

namespace Taijj.SumoTobey
{
    /// <summary>
    /// Facade for all the game's UI Elements.
    /// </summary>
    public class UserInterface : MonoBehaviour
    {
        [SerializeField] private StartScreen m_startScreen;
        [SerializeField] private GameScreen m_gameScreen;
        [SerializeField] private EndScreen m_endScreen;

        public void wake()
        {
            m_startScreen.wake();
            m_gameScreen.wake();
            m_endScreen.wake();
        }

        public StartScreen startScreen => m_startScreen;
        public GameScreen gameScreen => m_gameScreen;
        public EndScreen endScreen => m_endScreen;
    }
}