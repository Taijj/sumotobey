﻿using UnityEngine;

namespace Taijj.Utils
{
    /// <summary>
    /// Makes it possible to exit the game with the Esc Key.
    /// </summary>
    public class ExitHandler : SceneSingleton<ExitHandler>
    {
        public void Activate()
        {
            UpdateCaller.instance.startCallingOnUpdate(checkForExit);
        }

        private void checkForExit()
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                exit();
            }
        }

        private void exit()
        {
            #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
            #else
                Application.Quit();
            #endif
        }
    }
}