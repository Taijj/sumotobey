﻿namespace Taijj.Utils
{
    public static class ExtensionMethods
    {
        public static float fSquared(this float fTarget)
        {
            return fTarget*fTarget;
        }
    }
}