﻿using UnityEngine;

namespace Taijj.Utils
{
    /// <summary>
    /// Encapsulates Debug.Log() calls and prevents them from
    /// showing up in builds.
    /// </summary>
    public class Logger
    {
        public static void log(object _object)
        {
            #if UNITY_EDITOR || DEVELOPMENT_BUILD
                Debug.Log(_object);
            #endif
        }

        public static void logError(object _object)
        {
            #if UNITY_EDITOR || DEVELOPMENT_BUILD
                Debug.LogError(_object);
            #endif
        }

        public static void logWarning(object _object)
        {
            #if UNITY_EDITOR || DEVELOPMENT_BUILD
                Debug.LogWarning(_object);
            #endif
        }
    }
}