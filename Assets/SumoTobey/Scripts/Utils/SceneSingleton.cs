﻿using UnityEngine;

namespace Taijj.Utils
{
    /// <summary>
    /// Generic base for every object that resides in the scene, and should be accessible
    /// from anywhere.
    ///
    /// Is DontDestroyOnLoad!
    /// </summary>
    public abstract class SceneSingleton<T> : MimiBehaviour where T : SceneSingleton<T>
    {
        private static T m_instance;
        public static T instance
        {
            get
            {
                tryWakeInstance();
                return m_instance;
            }
        }

        private static void tryWakeInstance()
        {
            if(bHasBeenWoken)
            {
                return;
            }

            if(bInstanceIsMissing)
            {
                m_instance = (T)UnityEngine.Object.FindObjectOfType(typeof(T));
            }
            if(bInstanceIsMissing)
            {
                GameObject go = new GameObject(typeof(T).Name);
                m_instance = go.AddComponent<T>();
            }

            m_instance.wake();
        }

        protected virtual void wake()
        {
            DontDestroyOnLoad(gameObject);
            bHasBeenWoken = true;
        }

        private static bool bHasBeenWoken { get; set; }
        private static bool bInstanceIsMissing => ReferenceEquals(m_instance, null);
    }
}