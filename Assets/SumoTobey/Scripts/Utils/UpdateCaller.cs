﻿using System.Collections.Generic;
using System.Collections;
using System;
using UnityEngine;

namespace Taijj.Utils
{
    /// <summary>
    /// Centralized Behaviour for Unity Update() calls.
    /// </summary>
    public class UpdateCaller : SceneSingleton<UpdateCaller>
    {
        #region Registration
        public void startCallingOnUpdate(Action _del)
        {
            bool bIsNotCalledYet = s_liMethods.Contains(_del) == false;
            if (bIsNotCalledYet)
            {
                StartCoroutine(waitThenAdd(_del));
            }
        }

        private IEnumerator waitThenAdd(Action _del)
        {
            yield return new WaitForEndOfFrame();
            s_liMethods.Add(_del);
        }

        public void stopCallingOnUpdate(Action _del)
        {
            bool bIsCalled = s_liMethods.Contains(_del);
            if (bIsCalled)
            {
                StartCoroutine(waitThenRemove(_del));
            }
        }

        private IEnumerator waitThenRemove(Action _del)
        {
            yield return new WaitForEndOfFrame();
            s_liMethods.Remove(_del);
        }
        #endregion



        #region Update Loop
        public void Update()
        {
            s_liMethods.ForEach(m => m());
        }

        private List<Action> s_liMethods = new List<Action>();
        #endregion
    }
}