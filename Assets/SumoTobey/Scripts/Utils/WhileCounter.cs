﻿using System;

namespace Taijj.Utils
{
    /// <summary>
    /// Tool to catch infinite while loops.
    /// </summary>
    public class WhileCounter
    {
        public static void OnWhileLoopStart(int _iMaxIterations)
        {
            iCount = _iMaxIterations;
        }

        public static void OnWhileLoopIteration()
        {
            iCount--;
            if(iCount <= 0)
            {
                throw new Exception("WhileCounter reached zero. While loop busted!");
            }
        }

        private static int iCount { get; set; }
    }
}